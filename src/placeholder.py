#!/usr/bin/env python3
 
import rospy
import traceback 
import numpy as np
import parse_svg_for_robot_arm_v02 as psvg
# IMPORT the custom message: 
# we import it "from" the ROS package we created it in (here "me439robot") with an extension of .msg ...
# and actually import the message type by name (here "ME439WaypointXY")
from armrob_util.msg import ME439WaypointXYZ
from std_msgs.msg import Bool, Int32


# =============================================================================
# # Set waypoints to hit along the path 
# =============================================================================
previous_command = None
# Get parameters from rosparam
#
#filename = rospy.get_param('/path_svg_file')
filename = None
filename_options = ["/home/pi/svgs/write_0", "/home/pi/svgs/write_1", "/home/pi/svgs/write_2", "/home/pi/svgs/write_3", "/home/pi/svgs/write_4"]
print(filename_options)

#Get Height offset for safety (do practice runs in the air)
vertical_offset = rospy.get_param('/vertical_offset')

waypoints = None
next_waypoints = None


## Or set them manually:
## Waypoints to hit: a "numpy.array" of [x,y,z] coordinates. 
## Example: Square
#waypoints = np.array([[0.5, 0.,0.],[0.5,0.5,0.],[0.,0.5,0.],[0.,0.,0.]]) 

# =============================================================================
# # END of section on waypoint setting
# =============================================================================


##################################################################
# Run the Publisher
##################################################################
# initialize the current "segment" to be the first one (index 0) # (you could skip segments if you wanted to)
waypoint_number = 0  # for waypoint seeking. 
path_complete = Bool()
# Set path complete to default to true to not run any commands if there is not a command
path_complete.data = True
# Create the publisher for the topic "waypoint_xyz", with message type "ME439WaypointXYZ"
pub_waypoint_xyz = rospy.Publisher('/waypoint_xyz', ME439WaypointXYZ, queue_size=1)

# Create the publisher for the topic "path_complete", with message type "Bool"
pub_path_complete = rospy.Publisher('/path_complete', Bool, queue_size=1)



# Publish desired waypoints at the appropriate time. 
def talker(): 
    global next_waypoints, waypoints, waypoint_number, path_complete, pub_waypoint, pub_path_complete
    # Launch a node called "set_waypoints"
    rospy.init_node('set_waypoints', anonymous=False)
 
        # Create a subscriber that listens for messages on the "waypoint_complete" topic
    sub_waypoint_complete = rospy.Subscriber('/waypoint_complete', Bool, increment_waypoint)
    sub_command = rospy.Subscriber('/hand_gesture_num', Int32, set_file_to_run)
    # Declare the message to publish. 
    # Here we use one of the message name types we Imported, and add parentheses to call it as a function. 
    # We could also put data in it right away using . 
    msg_waypoint = ME439WaypointXYZ()
   

    
    # set up a rate basis to keep it on schedule.
    r = rospy.Rate(10) # N Hz
    while not rospy.is_shutdown():
        try: 
            
            # start a loop 
            while not rospy.is_shutdown() and not path_complete.data:
                msg_waypoint.xyz = waypoints[waypoint_number]
                
                # Actually publish the message
                pub_waypoint_xyz.publish(msg_waypoint)
                # Log the info (optional)
    #            rospy.loginfo(msg_waypoint)    
                
                pub_path_complete.publish(path_complete)
                
                r.sleep()


        except Exception:
            traceback.print_exc()
            pass
            
        # When it gets here, the loop is broken by the path being complete. Publish that fact. 
        pub_path_complete.publish(path_complete)

        if(next_waypoints != None):
            waypoints = next_waypoints
            next_waypoints = None
            path_complete.data = False
            r.sleep(2)
        
        


def increment_waypoint(msg_in):
    # get access to the globals set at the top
    global waypoint_number, path_complete, pub_waypoint, pub_path_complete
    
    if msg_in.data :  # it's Boolean
        waypoint_number = waypoint_number + 1
    
    if waypoint_number >= waypoints.shape[0]:
        path_complete.data = True
        waypoint_number = waypoint_number - 1  # This line prevents an array out of bounds error to make sure the node stayed alive. If commented, allow it to increment past the end, which will throw an exception (array out of bounds) the next time it publishes a waypoint and cause the node to die. 
    else:
        path_complete.data = False
    
    pub_path_complete.publish(path_complete)

def stop():
    path_complete.data = True

def set_file_to_run(msg_in):
    print("Set_file_to_run")
    try:
        #typecast the command
        command = int(msg_in)
    except:
        print("could not typecast to int")
        return
    #Check that command has changed
    if (previous_command == command):
        return
    #check that command is in bounds
    if ((command < 0) or (command > 5)):
        print("Invalid command")
        return
    if (command == 0):
        filename = filename_options[0]
    elif (command == 1):
        filename = filename_options[1]
    elif (command == 2):
        filename = filename_options[2]
    elif (command == 3):
        filename = filename_options[3]
    elif (command == 4):
        filename = filename_options[4]
    elif (command == 5):
        #if someone shows a 5, stop the device
        stop()
    # Use an SVG file to specify the path: 
    next_waypoints = psvg.convert_svg_to_waypoints(filename, xlength=0.08, ylength=0.08, rmin=0.18, rmax=0.26)
    next_waypoints = np.vstack( (np.array([0.2015, 0.0, 0.2056]), next_waypoints, np.array([0.2015, 0.0, 0.2056]), np.array([0.2015, 0.0, 0.2056])))
    print("next_waypoints set")
    next_waypoints[:,2] = next_waypoints[:,2]+vertical_offset
    

if __name__ == '__main__':
    try: 
        talker()
    except rospy.ROSInterruptException: 
        pass

