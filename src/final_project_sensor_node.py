#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#from *** import get_num   # import hand gesture recognition module
#import serial
import rospy
import traceback
# Import the message types we will need
from std_msgs.msg import Int32
import socket

HOST = '10.141.68.2' # Server IP or Hostname
PORT = 12345 # Pick an open Port (1000+ recommended), must match the client sport
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print('Socket created')


def sensors_reader(): 
    # Launch a node called "sensors_node"
    rospy.init_node('sensors_node', anonymous=False)

    # Create the publishers. Name each topic "sensors_##", with message type "Int32" 
    # (because that's what is received over the serial port)
    # Note the queue_size=1 statement: don't let it develop a backlog! 
    
    # Example: A0
    pub_A0 = rospy.Publisher('/hand_gesture_num', Int32, queue_size=1)
    
    # Other examples: 
    # pub_E0 = rospy.Publisher('/sensors_E0', Int32, queue_size=1)
    # pub_E1 = rospy.Publisher('/sensors_E1', Int32, queue_size=1)
    
    # Declare the message that will go on the topic. 
    # Here we use one of the message name types we Imported, and add parentheses to call it as a function. 
    # We put data in it using the .data field of the message.
    msg_A0 = Int32()
    
    # Other examples
#    msg_E0 = Int32()
#    msg_E1 = Int32()
    

# Data comes in on the Serial port. Set that up and start it. 

    #----------setup serial--------------
    #ser = serial.Serial('/dev/ttyUSB0')  #serial port to alamode is /dev/ttyS0. # port to Arduino Nano is /dev/ttyUSB0 
    #ser.baudrate = 57600
    #ser.bytesize = 8
    #ser.parity = 'N'
    #ser.stopbits = 1
    #ser.timeout = 1 # one second time out. 

    #ser.flush()  # Flush any data currently on the port
    #ser.readline()
    

    #------------setup socket------------
    #managing error exception
    try:
        s.bind((HOST, PORT))

    except socket.error:
        print('Bind failed')

    s.listen(5)
    print('Socket awaiting messages')
    (conn, addr) = s.accept()
    print('Connected')

    # MAIN LOOP to keep loading the message with new data. 
    # NOTE that at the moment the data are coming from a separate thread, but this will be replaced with the serial port line reader in the future. 
    while not rospy.is_shutdown():

        try:
            #data = ser.readline().decode().strip()
            data = conn.recv(1024).decode()
            print(data)
            pub_A0.publish(Int32(int(data)))
           # Here we read the serial port for a string that looks like "e0:123456", which is an Encoder0 reading. 
            # When we get a reading, update the associated motor command
            #line = ser.readline().decode().strip() #blocking function, will wait until read entire line
#            print(line)
            #line = line.split(":")
            # Element 0 of "line" will be a string that says what the data are: 
            #data_type = line[0]
            # Element 1 of "line" will be the value, an Integer
            #data_value = int(line[1])
#            print(data_type)
#            print(line)
            #if data_type == 'A0':
             #   msg_A0 = data_value	# Analog reading 
              #  pub_A0.publish(msg_A0)
            #else:
             #   continue
            
        
        except :
            traceback.print_exc()
            pass
            



if __name__ == '__main__':
    try: 
        sensors_reader()
    except  : 
        traceback.print_exc()
        pass
